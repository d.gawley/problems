import functools
import rich

def load_words(path='/usr/share/dict/words'):
    f = open(path, 'r')
    lines = f.readlines()
    return set(s for w in lines if (s := w[:-1]).isalpha() and s.islower())


def solve(text, dict_words):
    maxlen = max(len(w) for w in dict_words)

    @functools.cache
    def words_here(start):
        segment = text[start:]
        return [word for idx in range(1, min(start + maxlen, len(segment)) + 1)
                if (word := segment[:idx]) in dict_words]

    @functools.cache
    def paths_from(start):
        result = []
        for word in words_here(start):
            if (start + len(word) == len(text)):
                result.append([word])
            else:
                paths = paths_from(start + len(word))
                result += [[word] + p for p in paths]
        return result

    return paths_from(0)
